const express = require('express')

//UM PEQUENO FAKE DATABASE//
let books = []

// CRIANDO O APP no CASO O CORPO DO PROGRAMA//

const app = express ()
app.use(express.json());

app.post('/books', (req, res)=> {
    const { id, title, author, publishedAlt } = req.body;
    const book = { id, title, author, publishedAlt };
    books.push(book);
    return res.status(201).json(book);
});

app.get('/books', (req, res) => {
    const allBooks = books;
    return res.status(200).json(allBooks);

});
app.get('/books/:book_id', (req, res) => {
    const { book_id } =req.params
    const book = books.find((books) => book.id === book_id);
    if(!book) res.status(404).json("not found");
    return res.status(200).json(book);
});

app.delete('/books/:book_id', (req, res) => {
    const { book_id} = req.params
    const filteredBooks = books.filter(book => book.id !== book_id);
    books - filteredBooks;
    return res.status(204);
});

app.path("/books/:book_id", (req, res) => {
    const {author, title, publishedAlt } = req.body
    const { book_id } = req.params
    const book = books.find((book) => book.id === book_id);
    book.id = book.id;
    book.title = title ? title : book.title;
    book.author = author ? author : book.author;
    book.publishedAlt = publishedAlt ? publishedAlt : book.publishedAlt;
    return res.status(200).json(book);
});

//MANDAR O SERVIDOR RODAR PODE-SE USAR LOCALHOST//
app.listen(3333, () => console.log("Server is running"));

//FEITO USANDO DASHBOARD PRA RODALO,NODE.JS E EXPRESS!//